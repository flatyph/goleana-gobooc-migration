package main

import (
	"database/sql"
	"log"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
)

// type HotelAttraction2 struct {
// 	ID        int     `gorm:"column:id" json:"id"`
// 	HotelID   string  `gorm:"column:hotel_id" json:"hotel_id"`
// 	Name      string  `gorm:"column:name" json:"name"`
// 	Category  string  `gorm:"column:category" json:"category"`
// 	Lat       float64 `gorm:"column:lat" json:"lat"`
// 	Lon       float64 `gorm:"column:lon" json:"lon"`
// 	Distance  int     `gorm:"column:distance" json:"distance"`
// 	OtherInfo string  `gorm:"column:other_info" json:"other_info"`
// 	Status    string  `gorm:"column:status" json:"status"`
// }

type HotelRoomFacility struct {
	ID          int    `gorm:"column:id" json:"id"`
	HotelRoomID string `gorm:"column:hotel_room_id" json:"hotel_room_id"`
	Name        string `gorm:"column:name" json:"name"`
	Status      int    `gorm:"column:status" json:"status"`
	DateCreated string `gorm:"column:date_created" json:"date_created"`
}

var (
	// gobooc, goleana *sql.DB
	offset = 0 //67600000 //0
	limit  = 200000
	lastID = 0 //114531470 //67600000
)

func goboocConnect() *sql.DB {
	var err error

	gobooc, err := sql.Open("mysql", "hanzel:"+"root"+"@tcp("+"52.59.200.134"+":"+"3306"+")/"+"gobooc_migration")
	if err != nil {
		panic(err.Error())
	}

	gobooc.SetMaxOpenConns(0)

	if err != nil {
		log.Fatal(err)
	}

	return gobooc
}

func goleanaConnect() *sql.DB {
	var err error

	goleana, err := sql.Open("mysql", "adnanV2:"+"34#%nM*qRP^vG6HSrHdmBdz3y"+"@tcp("+"63.34.45.199"+":"+"3306"+")/"+"goleanaDemo")
	if err != nil {
		panic(err.Error())
	}

	if err != nil {
		log.Fatal(err)
	}

	goleana.SetMaxOpenConns(0)

	return goleana
}

func main() {
	goleana := goleanaConnect()

	log.Println("Querying Batch")
	log.Println("LastID Batch " + strconv.Itoa(lastID))

	rows, err := goleana.Query(`
		SELECT id, hotel_room_id, name, status, date_created
		FROM goleanaDemo.hotel_room_facility
		WHERE id > ` + strconv.Itoa(lastID) + `
		LIMIT ` + strconv.Itoa(limit))

	if err != nil {
		log.Println(err)
	}

	log.Println("Done Querying Hotel Room Facilities - Last_ID " + strconv.Itoa(lastID))

	hotelRoomFacilities := []HotelRoomFacility{}
	hotelRoomFacility := HotelRoomFacility{}

	var insForm *sql.Stmt

	for rows.Next() {

		log.Println("Storing Hotel Room Facilities To Struct - Last_ID " + strconv.Itoa(lastID))
		if err := rows.Scan(&hotelRoomFacility.ID, &hotelRoomFacility.HotelRoomID, &hotelRoomFacility.Name, &hotelRoomFacility.Status, &hotelRoomFacility.DateCreated); err != nil {
			log.Println(err.Error())
		}

		log.Println(hotelRoomFacility)

		hotelRoomFacilities = append(hotelRoomFacilities, hotelRoomFacility)
	}

	goleana.Close()

	log.Println("Done Storing Hotel Room Facilities To Struct")

	lastID = hotelRoomFacilities[len(hotelRoomFacilities)-1].ID

	log.Println("Starting To Concat Hotel Rooms Facilities " + strconv.Itoa(lastID))

	sqlStr := "INSERT IGNORE INTO hotel_room_facility (hotel_room_id, name, status, date_created) VALUES "

	ctr := 0
	ctr1 := 0
	vals := []interface{}{}

	for _, value := range hotelRoomFacilities {
		sqlStr += "(?,?,?,?),"
		ctr++
		ctr1++

		vals = append(vals,
			value.HotelRoomID,
			value.Name,
			value.Status,
			value.DateCreated)

		if ctr == 5000 || ctr1 == len(hotelRoomFacilities) {
			gobooc := goboocConnect()

			//Insert
			log.Println("INSERTING LAST_ID BATCH " + strconv.Itoa(lastID))

			sqlStr = sqlStr[0 : len(sqlStr)-1]

			insForm, err = gobooc.Prepare(sqlStr)

			if err != nil {
				panic(err.Error())
			}

			_, err = insForm.Exec(vals...)

			if err != nil {
				log.Println(err)
			}

			//Clearing
			sqlStr = "INSERT IGNORE INTO hotel_room_facility (hotel_room_id, name, status, date_created) VALUES "
			vals = []interface{}{}
			ctr = 0

			gobooc.Close()
		}
	}

	main()
}
