package main

import (
	"database/sql"
	"log"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
)

type ArHotel struct {
	ID            int     `gorm:"column:id" json:"id"`
	HotelID       int     `gorm:"column:hotel_id" json:"hotel_id"`
	GoCode        string  `gorm:"column:gocode" json:"gocode"`
	BookingID     string  `gorm:"column:booking_id" json:"booking_id"`
	HotelName     string  `gorm:"column:hotel_name" json:"hotel_name"`
	Star          string  `gorm:"column:star" json:"star"`
	Lat           float64 `gorm:"column:lat" json:"lat"`
	Lon           float64 `gorm:"column:lon" json:"lon"`
	CityName      string  `gorm:"column:city_name" json:"city_name"`
	CountryName   string  `gorm:"column:country_name" json:"country_name"`
	Address       string  `gorm:"column:address" json:"address"`
	ZipCode       string  `gorm:"column:zip_code" json:"zip_code"`
	DateCreated   string  `gorm:"column:date_created" json:"date_created"`
	BookingURL    string  `gorm:"column:booking_url" json:"booking_url"`
	BookingRating float64 `gorm:"column:booking_rating" json:"booking_rating"`
	BookingUI     string  `gorm:"column:booking_ui" json:"booking_ui"`
	TripadvisorUI string  `gorm:"column:tripadvisor_ui" json:"tripadvisor_ui"`
	BookingS3URL  string  `gorm:"column:booking_s3_url" json:"booking_s3_url"`
	PropertyType  string  `gorm:"column:property_type" json:"property_type"`
	PropertyStyle string  `gorm:"column:property_style" json:"property_style"`
	Phone         string  `gorm:"column:phone" json:"phone"`
	Fax           string  `gorm:"column:fax" json:"fax"`
	CheckIn       string  `gorm:"column:check_in" json:"check_in"`
	CheckOut      string  `gorm:"column:check_out" json:"check_out"`
	Website       string  `gorm:"column:website" json:"website"`
	Pet           string  `gorm:"column:pet" json:"pet"`
	Chain         string  `gorm:"column:chain" json:"chain"`
	Payment       string  `gorm:"column:payment" json:"payment"`
	Email         string  `gorm:"column:email" json:"email"`
	Room          string  `gorm:"column:room" json:"room"`
	ExistSince    string  `gorm:"column:exist_since" json:"exist_since"`
	Batch         string  `gorm:"column:batch" json:"batch"`
}

var (
	// gobooc, goleana *sql.DB
	offset = 0    //67600000 //0
	limit  = 2000 //200000
	lastID = 0    //114531470 //67600000
)

func goboocConnect() *sql.DB {
	var err error

	gobooc, err := sql.Open("mysql", "hanzel:"+"root"+"@tcp("+"52.59.200.134"+":"+"3306"+")/"+"gobooc_migration")
	if err != nil {
		panic(err.Error())
	}

	gobooc.SetMaxOpenConns(0)

	if err != nil {
		log.Fatal(err)
	}

	return gobooc
}

func goleanaConnect() *sql.DB {
	var err error

	goleana, err := sql.Open("mysql", "adnanV2:"+"34#%nM*qRP^vG6HSrHdmBdz3y"+"@tcp("+"63.34.45.199"+":"+"3306"+")/"+"goleanaDemo")
	if err != nil {
		panic(err.Error())
	}

	if err != nil {
		log.Fatal(err)
	}

	goleana.SetMaxOpenConns(0)

	return goleana
}

func main() {
	goleana := goleanaConnect()

	log.Println("Querying Batch")
	log.Println("LastID Batch " + strconv.Itoa(lastID))

	rows, err := goleana.Query(`
		SELECT 
		id, 
		hotel_id, 
		gocode,
		booking_id, 
		IFNULL(hotel_name, "") as hotel_name, 
		IFNULL(star, "") as star,
		lat, 
		lon,
		IFNULL(city_name, "") as city_name,
		IFNULL(country_name, "") as country_name,
		IFNULL(address, "") as address,
		IFNULL(zip_code, "") as zip_code,
		IFNULL(date_created, "") as date_created,
		IFNULL(booking_url, "") as booking_url, 
		IFNULL(booking_rating, 0.0) as booking_rating,
		IFNULL(booking_ui, "") as booking_ui,
		IFNULL(tripadvisor_ui, "") as tripadvisor_ui, 
		IFNULL(booking_s3_url, "") as booking_s3_url, 
		IFNULL(property_type, "") as property_type,
		IFNULL(property_style, "") as property_style,
		IFNULL(phone, "") as phone,
		IFNULL(fax, "") as fax,
		IFNULL(check_in, "") as check_out, 
		IFNULL(check_out, "") as check_out, 
		IFNULL(website, "") as website, 
		IFNULL(pet, "") as pet, 
		IFNULL(chain, "") as chain, 
		IFNULL(payment, "") as payment,
		IFNULL(email, "") as email,
		IFNULL(room, "") as room,
		IFNULL(exist_since, "") as exist_since,
		IFNULL(batch, "") as batch
		FROM goleanaDemo.ar_hotel
		WHERE id > ` + strconv.Itoa(lastID) + `
		LIMIT ` + strconv.Itoa(limit))

	if err != nil {
		log.Println(err)
	}

	log.Println("Done Querying AR Hotels - Last_ID " + strconv.Itoa(lastID))

	arHotels := []ArHotel{}
	arHotel := ArHotel{}

	var insForm *sql.Stmt

	for rows.Next() {
		log.Println("Storing AR Hotels To Struct - Last_ID " + strconv.Itoa(lastID))
		if err := rows.Scan(
			&arHotel.ID,
			&arHotel.HotelID,       //1
			&arHotel.GoCode,        //2
			&arHotel.BookingID,     //3
			&arHotel.HotelName,     //4
			&arHotel.Star,          //5
			&arHotel.Lat,           //6
			&arHotel.Lon,           //7
			&arHotel.CityName,      //8
			&arHotel.CountryName,   //9
			&arHotel.Address,       //10
			&arHotel.ZipCode,       //11
			&arHotel.DateCreated,   //12
			&arHotel.BookingURL,    //13
			&arHotel.BookingRating, //14
			&arHotel.BookingUI,     //15
			&arHotel.TripadvisorUI, //16
			&arHotel.BookingS3URL,  //17
			&arHotel.PropertyType,  //18
			&arHotel.PropertyStyle, //19
			&arHotel.Phone,         //20
			&arHotel.Fax,           //21
			&arHotel.CheckIn,       //22
			&arHotel.CheckOut,      //23
			&arHotel.Website,       //24
			&arHotel.Pet,           //25
			&arHotel.Chain,         //26
			&arHotel.Payment,       //27
			&arHotel.Email,         //28
			&arHotel.Room,          //29
			&arHotel.ExistSince,    //30
			&arHotel.Batch,         //31
		); err != nil {
			log.Println(err.Error())
		}

		// log.Println(arHotel)

		arHotels = append(arHotels, arHotel)
	}

	goleana.Close()

	log.Println("Done Storing AR Hotels To Struct")

	lastID = arHotels[len(arHotels)-1].ID

	log.Println("Starting To Concat Hotel Rooms " + strconv.Itoa(lastID) + " len(arHotels) " + strconv.Itoa(len(arHotels)))

	sqlStr := `INSERT IGNORE INTO ar_hotel (
		hotel_id,
		gocode,
		booking_id,
		hotel_name,
		star,
		lat,
		lon,
		city_name,
		country_name,
		address,
		zip_code,
		date_created,
		booking_url,
		booking_rating,
		booking_ui,
		tripadvisor_ui,
		booking_s3_url,
		property_type,
		property_style,
		phone,
		fax,
		check_in,
		check_out,
		website,
		pet,
		chain,
		payment,
		email,
		room,
		exist_since,
		batch
	) VALUES `

	//31 Total
	ctr := 0
	ctr1 := 0
	vals := []interface{}{}

	for _, value := range arHotels {
		sqlStr += "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?),"
		ctr++
		ctr1++

		vals = append(vals,
			value.HotelID,       //1
			value.GoCode,        //2
			value.BookingID,     //3
			value.HotelName,     //4
			value.Star,          //5
			value.Lat,           //6
			value.Lon,           //7
			value.CityName,      //8
			value.CountryName,   //9
			value.Address,       //10
			value.ZipCode,       //11
			value.DateCreated,   //12
			value.BookingURL,    //13
			value.BookingRating, //14
			value.BookingUI,     //15
			value.TripadvisorUI, //16
			value.BookingS3URL,  //17
			value.PropertyType,  //18
			value.PropertyStyle, //19
			value.Phone,         //20
			value.Fax,           //21
			value.CheckIn,       //22
			value.CheckOut,      //23
			value.Website,       //24
			value.Pet,           //25
			value.Chain,         //26
			value.Payment,       //27
			value.Email,         //28
			value.Room,          //29
			value.ExistSince,    //30
			value.Batch,         //31
		)

		if ctr == 5000 || ctr1 == len(arHotels) {
			gobooc := goboocConnect()

			//Insert
			log.Println("INSERTING LAST_ID BATCH " + strconv.Itoa(lastID))

			sqlStr = sqlStr[0 : len(sqlStr)-1]

			insForm, err = gobooc.Prepare(sqlStr)

			if err != nil {
				panic(err.Error())
			}

			_, err = insForm.Exec(vals...)

			if err != nil {
				log.Println(err)
			}

			//Clearing
			sqlStr = `INSERT IGNORE INTO ar_hotel (
				hotel_id,
				gocode,
				booking_id,
				hotel_name,
				star,
				lat,
				lon,
				city_name,
				country_name,
				address,
				zip_code,
				date_created,
				booking_url,
				booking_rating,
				booking_ui,
				tripadvisor_ui,
				booking_s3_url,
				property_type,
				property_style,
				phone,
				fax,
				check_in,
				check_out,
				website,
				pet,
				chain,
				payment,
				email,
				room,
				exist_since,
				batch
			) VALUES `
			vals = []interface{}{}
			ctr = 0

			gobooc.Close()
		}
	}

	main()
}
