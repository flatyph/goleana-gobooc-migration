package main

import (
	"database/sql"
	"log"
	"os"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
)

// type HotelAttraction2 struct {
// 	ID        int     `gorm:"column:id" json:"id"`
// 	HotelID   string  `gorm:"column:hotel_id" json:"hotel_id"`
// 	Name      string  `gorm:"column:name" json:"name"`
// 	Category  string  `gorm:"column:category" json:"category"`
// 	Lat       float64 `gorm:"column:lat" json:"lat"`
// 	Lon       float64 `gorm:"column:lon" json:"lon"`
// 	Distance  int     `gorm:"column:distance" json:"distance"`
// 	OtherInfo string  `gorm:"column:other_info" json:"other_info"`
// 	Status    string  `gorm:"column:status" json:"status"`
// }

type HotelPhotos struct {
	ID          int    `gorm:"column:id" json:"id"`
	HotelID     string `gorm:"column:hotel_id" json:"hotel_id"`
	Source      string `gorm:"column:source" json:"source"`
	Description string `gorm:"column:description" json:"description"`
	Type        string `gorm:"column:type" json:"type"`
	Tags        string `gorm:"column:tags" json:"tags"`
	SourceLink  string `gorm:"column:source_link" json:"source_link"`
	IsActive    int    `gorm:"column:is_active" json:"is_active"`
}

var (
	// gobooc, goleana *sql.DB
	offset = 0  //67600000 //0
	limit  = 20 //200000
	lastID = 0  //114531470 //67600000
)

func goboocConnect() *sql.DB {
	var err error

	gobooc, err := sql.Open("mysql", "hanzel:"+"root"+"@tcp("+"52.59.200.134"+":"+"3306"+")/"+"gobooc_migration")
	if err != nil {
		panic(err.Error())
	}

	gobooc.SetMaxOpenConns(0)

	if err != nil {
		log.Fatal(err)
	}

	return gobooc
}

func goleanaConnect() *sql.DB {
	var err error

	goleana, err := sql.Open("mysql", "adnanV2:"+"34#%nM*qRP^vG6HSrHdmBdz3y"+"@tcp("+"63.34.45.199"+":"+"3306"+")/"+"goleanaDemo")
	if err != nil {
		panic(err.Error())
	}

	if err != nil {
		log.Fatal(err)
	}

	goleana.SetMaxOpenConns(0)

	return goleana
}

func main() {
	goleana := goleanaConnect()
	log.Println("QUERYING OFFSET: " + strconv.Itoa(offset))

	log.Println("Querying Batch")
	log.Println("LastID Batch " + strconv.Itoa(lastID))

	rows, err := goleana.Query(`SELECT
						gr.id as id,
                        IFNULL(gd.gocode, "") as hotel_id,
                        gr.source as source,
						gr.description as description,
						gr.type as type,
						gr.tags as tags,
						gr.source_link as source_link,
						gr.is_active as is_active,
                        FROM goleanaDemo.hotel_photos gr
                        LEFT JOIN goleanaDemo.hotel gd
						ON gd.hotel_id = gr.hotel_id
						WHERE gr.id > ` + strconv.Itoa(lastID) + `
						ORDER BY gr.id
                        LIMIT ` + strconv.Itoa(limit))

	if err != nil {
		log.Println(err)
	}

	log.Println("Done Querying Hotel Photos - Last_ID " + strconv.Itoa(lastID))

	hotelPhotos := []HotelPhotos{}
	hotelPhoto := HotelPhotos{}

	var insForm *sql.Stmt

	for rows.Next() {

		if err := rows.Scan(&hotelPhoto.ID, &hotelPhoto.HotelID, &hotelPhoto.Source, &hotelPhoto.Description, &hotelPhoto.Type, &hotelPhoto.Tags, &hotelPhoto.SourceLink, &hotelPhoto.IsActive); err != nil {
			log.Println(err.Error())
		}

		log.Println(hotelPhoto)

		hotelPhotos = append(hotelPhotos, hotelPhoto)
	}

	goleana.Close()

	os.Exit(0)

	log.Println("Done Storing Hotel Photos To Struct")

	lastID = hotelPhotos[len(hotelPhotos)-1].ID

	log.Println("Starting To Concat Hotel Photos " + strconv.Itoa(lastID))

	sqlStr := "INSERT IGNORE INTO hotel_photos2 (id, hotel_id, source, description, type, tags, source_link, is_active) VALUES "

	ctr := 0
	ctr1 := 0
	vals := []interface{}{}

	for _, value := range hotelPhotos {
		sqlStr += "(?,?,?,?,?,?,?,?,?,?),"
		ctr++
		ctr1++

		vals = append(vals,
			value.ID,
			value.HotelID,
			value.Source,
			value.Description,
			value.Type,
			value.Tags,
			value.SourceLink,
			value.IsActive,
		)

		if ctr == 5000 || ctr1 == len(hotelPhotos) {
			os.Exit(0)

			gobooc := goboocConnect()

			//Insert
			log.Println("INSERTING LAST_ID BATCH " + strconv.Itoa(lastID))

			sqlStr = sqlStr[0 : len(sqlStr)-1]

			insForm, err = gobooc.Prepare(sqlStr)

			if err != nil {
				panic(err.Error())
			}

			_, err = insForm.Exec(vals...)

			if err != nil {
				log.Println(err)
			}

			//Clearing
			sqlStr = "INSERT IGNORE INTO hotel_photos2 (id, hotel_id, source, description, type, tags, source_link, is_active) VALUES "
			vals = []interface{}{}
			ctr = 0

			gobooc.Close()
		}
	}

	main()
}
