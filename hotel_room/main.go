package main

import (
	"database/sql"
	"log"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
)

// type HotelAttraction2 struct {
// 	ID        int     `gorm:"column:id" json:"id"`
// 	HotelID   string  `gorm:"column:hotel_id" json:"hotel_id"`
// 	Name      string  `gorm:"column:name" json:"name"`
// 	Category  string  `gorm:"column:category" json:"category"`
// 	Lat       float64 `gorm:"column:lat" json:"lat"`
// 	Lon       float64 `gorm:"column:lon" json:"lon"`
// 	Distance  int     `gorm:"column:distance" json:"distance"`
// 	OtherInfo string  `gorm:"column:other_info" json:"other_info"`
// 	Status    string  `gorm:"column:status" json:"status"`
// }

type HotelRoom struct {
	ID          int    `gorm:"column:id" json:"id"`
	HotelID     string `gorm:"column:hotel_id" json:"hotel_id"`
	Name        string `gorm:"column:name" json:"name"`
	Size        int    `gorm:"column:size" json:"size"`
	Parking     string `gorm:"column:parking" json:"parking"`
	Wifi        string `gorm:"column:wifi" json:"wifi"`
	View        string `gorm:"column:view" json:"view"`
	Pax         string `gorm:"column:pax" json:"pax"`
	Type        string `gorm:"column:type" json:"type"`
	Status      int    `gorm:"column:status" json:"status"`
	DateCreated string `gorm:"column:date_created" json:"date_created"`
}

var (
	// gobooc, goleana *sql.DB
	offset = 0      //67600000 //0
	limit  = 200000 //200000
	lastID = 0      //114531470 //67600000
)

func goboocConnect() *sql.DB {
	var err error

	gobooc, err := sql.Open("mysql", "hanzel:"+"root"+"@tcp("+"52.59.200.134"+":"+"3306"+")/"+"gobooc_migration")
	if err != nil {
		panic(err.Error())
	}

	gobooc.SetMaxOpenConns(0)

	if err != nil {
		log.Fatal(err)
	}

	return gobooc
}

func goleanaConnect() *sql.DB {
	var err error

	goleana, err := sql.Open("mysql", "adnanV2:"+"34#%nM*qRP^vG6HSrHdmBdz3y"+"@tcp("+"63.34.45.199"+":"+"3306"+")/"+"goleanaDemo")
	if err != nil {
		panic(err.Error())
	}

	if err != nil {
		log.Fatal(err)
	}

	goleana.SetMaxOpenConns(0)

	return goleana
}

func main() {
	goleana := goleanaConnect()
	// log.Println("QUERYING OFFSET: " + strconv.Itoa(offset))

	log.Println("Querying Batch")
	log.Println("LastID Batch " + strconv.Itoa(lastID))

	rows, err := goleana.Query(`SELECT
						gr.id as id,
                        IFNULL(gd.gocode, "") as hotel_id,
                        gr.name as name,
						gr.size as size,
						gr.parking as parking,
						gr.wifi as wifi,
						gr.view as view,
						gr.pax as pax,
						gr.type as type,
                        gr.status as status,
                        gr.date_created as date_created
                        FROM goleanaDemo.hotel_room gr
                        LEFT JOIN goleanaDemo.hotel gd
						ON gd.hotel_id = gr.hotel_id
						WHERE gr.id > ` + strconv.Itoa(lastID) + `
						ORDER BY gr.id
                        LIMIT ` + strconv.Itoa(limit))

	if err != nil {
		log.Println(err)
	}

	log.Println("Done Querying Hotel Rooms - Last_ID " + strconv.Itoa(lastID))

	hotelRooms := []HotelRoom{}
	hotelRoom := HotelRoom{}

	var insForm *sql.Stmt

	for rows.Next() {

		if err := rows.Scan(&hotelRoom.ID, &hotelRoom.HotelID, &hotelRoom.Name, &hotelRoom.Size, &hotelRoom.Parking, &hotelRoom.Wifi, &hotelRoom.View, &hotelRoom.Pax, &hotelRoom.Type, &hotelRoom.Status, &hotelRoom.DateCreated); err != nil {
			log.Println(err.Error())
		}

		log.Println(hotelRoom)

		hotelRooms = append(hotelRooms, hotelRoom)
	}

	goleana.Close()

	log.Println("Done Storing Hotel Rooms To Struct")

	lastID = hotelRooms[len(hotelRooms)-1].ID

	log.Println("Starting To Concat Hotel Rooms " + strconv.Itoa(lastID))

	sqlStr := "INSERT IGNORE INTO hotel_room (hotel_id, name, size, parking, wifi, view, pax, type, status, date_created) VALUES "

	ctr := 0
	ctr1 := 0
	vals := []interface{}{}

	for _, value := range hotelRooms {
		sqlStr += "(?,?,?,?,?,?,?,?,?,?),"
		ctr++
		ctr1++

		vals = append(vals,
			value.HotelID,
			value.Name,
			value.Size,
			value.Parking,
			value.Wifi,
			value.View,
			value.Pax,
			value.Type,
			value.Status,
			value.DateCreated)

		if ctr == 5000 || ctr1 == len(hotelRooms) {
			gobooc := goboocConnect()

			//Insert
			log.Println("INSERTING LAST_ID BATCH " + strconv.Itoa(lastID))

			sqlStr = sqlStr[0 : len(sqlStr)-1]

			insForm, err = gobooc.Prepare(sqlStr)

			if err != nil {
				panic(err.Error())
			}

			_, err = insForm.Exec(vals...)

			if err != nil {
				log.Println(err)
			}

			//Clearing
			sqlStr = "INSERT IGNORE INTO hotel_room (hotel_id, name, size, parking, wifi, view, pax, type, status, date_created) VALUES "
			vals = []interface{}{}
			ctr = 0

			gobooc.Close()
		}
	}

	main()
}
