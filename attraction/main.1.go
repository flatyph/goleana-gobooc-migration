package main

import (
	"database/sql"
	"log"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
)

type HotelAttraction2 struct {
	ID        int     `gorm:"column:id" json:"id"`
	HotelID   string  `gorm:"column:hotel_id" json:"hotel_id"`
	Name      string  `gorm:"column:name" json:"name"`
	Category  string  `gorm:"column:category" json:"category"`
	Lat       float64 `gorm:"column:lat" json:"lat"`
	Lon       float64 `gorm:"column:lon" json:"lon"`
	Distance  int     `gorm:"column:distance" json:"distance"`
	OtherInfo string  `gorm:"column:other_info" json:"other_info"`
	Status    string  `gorm:"column:status" json:"status"`
}

var (
	// gobooc, goleana *sql.DB
	offset = 0 //67600000 //0
	limit  = 200000
	lastID = 114531470 //67600000
)

func goboocConnect() *sql.DB {
	var err error

	gobooc, err := sql.Open("mysql", "hanzel:"+"root"+"@tcp("+"52.59.200.134"+":"+"3306"+")/"+"gobooc_migration")
	if err != nil {
		panic(err.Error())
	}

	gobooc.SetMaxOpenConns(0)

	if err != nil {
		log.Fatal(err)
	}

	return gobooc
}

func goleanaConnect() *sql.DB {
	var err error

	goleana, err := sql.Open("mysql", "adnanV2:"+"34#%nM*qRP^vG6HSrHdmBdz3y"+"@tcp("+"63.34.45.199"+":"+"3306"+")/"+"goleanaDemo")
	if err != nil {
		panic(err.Error())
	}

	if err != nil {
		log.Fatal(err)
	}

	goleana.SetMaxOpenConns(0)

	return goleana
}

func main() {
	goleana := goleanaConnect()
	// log.Println("QUERYING OFFSET: " + strconv.Itoa(offset))
	//
	rows, err := goleana.Query(`SELECT
						gr.id as id,
                        IFNULL(gd.gocode, "") as hotel_id,
                        gr.name as name,
                        gr.category as category,
                        gr.lat as lat,
                        gr.lon as lon,
                        gr.distance as distance,
                        gr.other_info as other_info,
                        gr.status as status
                        FROM goleanaRND.hotel_nearby_attraction gr
                        LEFT JOIN goleanaDemo.hotel gd
						ON gd.hotel_id = gr.hotel_id
						WHERE gr.id > ` + strconv.Itoa(lastID) + `
                        LIMIT ` + strconv.Itoa(limit))

	if err != nil {
		log.Println(err)
	}

	attractions := []HotelAttraction2{}
	attraction := HotelAttraction2{}

	var insForm *sql.Stmt

	for rows.Next() {

		if err := rows.Scan(&attraction.ID, &attraction.HotelID, &attraction.Name, &attraction.Category, &attraction.Lat, &attraction.Lon, &attraction.Distance, &attraction.OtherInfo, &attraction.Status); err != nil {
			log.Println(err.Error())
		}

		attractions = append(attractions, attraction)
	}

	goleana.Close()

	lastID = attractions[len(attractions)-1].ID

	log.Println(lastID)

	sqlStr := "INSERT IGNORE INTO hotel_attraction2 (hotel_id, name, category, lat, lon, distance, other_info, status) VALUES "

	ctr := 0
	ctr1 := 0
	vals := []interface{}{}

	for _, value := range attractions {
		sqlStr += "(?,?,?,?,?,?,?,?),"
		ctr++
		ctr1++

		vals = append(vals,
			value.HotelID,
			value.Name,
			value.Category,
			strconv.FormatFloat(value.Lat, 'f', 8, 64),
			strconv.FormatFloat(value.Lon, 'f', 8, 64),
			strconv.Itoa(value.Distance),
			value.OtherInfo,
			value.Status)

		if ctr == 5000 || ctr1 == len(attractions) {
			gobooc := goboocConnect()

			//Insert
			log.Println("INSERTING LASTID BATCH " + strconv.Itoa(lastID))

			sqlStr = sqlStr[0 : len(sqlStr)-1]

			insForm, err = gobooc.Prepare(sqlStr)

			if err != nil {
				panic(err.Error())
			}

			_, err = insForm.Exec(vals...)

			if err != nil {
				log.Println(err)
			}

			//Clearing
			sqlStr = "INSERT IGNORE INTO hotel_attraction2 (hotel_id, name, category, lat, lon, distance, other_info, status) VALUES "
			vals = []interface{}{}
			ctr = 0

			gobooc.Close()
		}
	}

	main()
}
